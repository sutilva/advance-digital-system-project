library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

entity min_u is
port( clk, rstb,set: in std_logic;
			SW: in std_logic_vector(7 downto 0);
        cnt_9m: out std_logic;
		  minute_u: out std_logic_vector(3 downto 0));
end min_u;
architecture rtl of min_u is
signal s,a :std_logic_vector(3 downto 0);
signal count,cnt : std_logic_vector(3 downto 0);
signal se,c : std_logic;
begin
s<=SW(3)&SW(2)&SW(1)&SW(0);
--a<=SW(7)&SW(6)&SW(5)&SW(4);
--se<= not set;
a<=SW(7)&SW(6)&SW(5)&SW(4);
process(set)
begin
if((a>"0101"  or count>"1001") and  set='0') then
		cnt<="0000";
		c<='0';

		elsif(count="1001" and set = '0') then
		cnt<=s;
		c<='0';
		else
		cnt<=s;
		c<='0';
	end if;

end process;
--process(se)
--begin
--if((a>"0101"  or s>"1001") and  se='1') then
--		c<='0';
--		cnt<="0000";
--		elsif(s="1001" and se = '1') then
--		cnt<=s;
--		c<='1';
--		else
--		cnt<=s;
--		c<='0';
--	end if;
--
--end process;
process(clk,rstb,set)
begin
if(rstb='0') then
	count<="0000";
	cnt_9m<='0';
 elsif(set='0') then
    count<=cnt;
		cnt_9m<=c;
	 elsif(rising_edge(clk))then
	    if(count>="1001") then
			count<="0000";
			cnt_9m<='1';
			else
				count<=count + '1';
				cnt_9m<='0';
			end if;
end if;
end process;

minute_u<=count;
--cnt_9m<=c;
end rtl;
