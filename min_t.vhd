library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

entity min_t is
port( clk, rstb,set: in std_logic;
			SW: in std_logic_vector(7 downto 0);
        cnt_5m: out std_logic;
		  minute_t: out std_logic_vector(3 downto 0));
end min_t;
architecture rtl of min_t is
signal s,a :std_logic_vector(3 downto 0);
signal count,cnt : std_logic_vector(3 downto 0);
signal se ,c: std_logic;
begin
--s<=SW(3)&SW(2)&SW(1)&SW(0);
a<=SW(7)&SW(6)&SW(5)&SW(4);
s<=SW(3)&SW(2)&SW(1)&SW(0);
process(set)
begin
if((a>"0101"  or s>"1001") and  set='0') then
		c<='0';
	cnt<="0000";
elsif(a="0101" and set='0') then
		cnt<=a;
		c<='0';

		else
		cnt<=a;
		c<='0';
	end if;

end process;

--se<= not set;
--process(se)
--begin
--if((a>"0101"  or s>"1001") and  se='1') then
--		c<='0';
--		cnt<="0000";
--		elsif(a="0101") then
--		cnt<=a;
--		c<='1';
--		else
--		cnt<=a;
--		c<='0';
--	end if;

--end process;
process(clk,rstb,set)
begin
if(rstb='0') then
	count<="0000";
	cnt_5m<='0';
elsif(set='0') then
    count<=cnt;
	 cnt_5m<=c;
	 elsif(rising_edge(clk))then
	    if(count>="0101") then
			count<="0000";
			cnt_5m<='1';
			else
				count<=count + '1';
				cnt_5m<='0';
			end if;

end if;

end process;

minute_t<=count;
--cnt_5m<='0';
end rtl;
