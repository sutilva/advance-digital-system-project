library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

entity hr_t is
  port( clk, rstb,set: in std_logic;
			SW: in std_logic_vector(7 downto 0);
        cnt_2hr: out std_logic;
		  hour_t: out std_logic_vector(3 downto 0));
end hr_t;
architecture rtl of hr_t is
signal s,a :std_logic_vector(3 downto 0);
signal count,cnt : std_logic_vector(3 downto 0);
signal se ,c: std_logic;
begin
s<=SW(3)&SW(2)&SW(1)&SW(0);
a<="00"&SW(5)&SW(4);

se<= not set;
process(set)
begin
if(((a>="0010" and s>"0011") or (a<"0010" and s>"1001")) and  set='0') then
		c<='0';
		cnt<="0000";
		elsif(a="0010") then 
		cnt<=a;
		c<='1';
		else
		cnt<=a;
		c<='0';
	end if;	
   
--end process;
end process;
	

--process(se)
--begin
--if((a>"0010" or s>"1001") and  se='1') then
--		c<='0';
--		cnt<="0000";
--		elsif(a="0010") then 
--		cnt<=a;
--		c<='1';
--		else
--		cnt<=a;
--		c<='0';
--	end if;	
--   
--end process;

process(clk,rstb,set)
begin
if(rstb='0') then
	count<="0000";
 elsif(set='0') then
 count<=cnt;
 cnt_2hr<=c;
 --cnt_2hr<=c;
--	if(s>"0010" and  se='1') then
--		cnt_2hr<='0';
--		count<="0000";
--		elsif(s="0010") then 
--		count<=s;
--		cnt_2hr<='1';
--		else
--		count<=s;
--		cnt_2hr<='0';
--		end if;
   
	 elsif(rising_edge(clk))then 
	    if(count>="0010") then
			count<="0000";
			cnt_2hr<='1';
			else
				count<=count + '1';
				cnt_2hr<='0';
			end if;
		end if;
end process;
hour_t<=count;
end rtl;
