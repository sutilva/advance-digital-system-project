library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

entity sec_t is
  port( clk, rstb,set: in std_logic;
        cnt_5s: out std_logic;
		  second_t: out std_logic_vector(3 downto 0));
end sec_t;
architecture rtl of sec_t is
signal count : std_logic_vector(3 downto 0);
begin

process(clk,rstb,set)
begin
if(rstb='0') then
	count<="0000";
	cnt_5s<='0';
 elsif(set='0') then
    count<="0000";
	 cnt_5s<='0';
	 elsif(rising_edge(clk))then 
	    if(count>="0101") then
			count<="0000";
			cnt_5s<='1';
			else
				count<=count + '1';
				cnt_5s<='0';
			end if;

	
end if;

end process;	
second_t<=count;
end rtl;
