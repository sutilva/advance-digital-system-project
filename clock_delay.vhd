library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
entity clock_delay is
port (
   clk_50hz: in std_logic;
   clk_1sec: out std_logic
  );
end clock_delay;
architecture Behavioral of clock_delay is
signal counter: std_logic_vector(25 downto 0) := "00000000000000000000000000";
begin
process (clk_50hz)
 begin

  if(rising_edge(clk_50hz)) then
   counter <= counter + 1;

   if(counter>="00000000000000000000000010") then
		clk_1sec<='0';
       counter <= "00000000000000000000000000";
		 elsif(counter>="00000000000000000000000001") then
		 clk_1sec<='1';

  end if;
  end if;
 end process;
--clk_1sec <= '0' when counter < "010111110101111000010000000" else '1';
end Behavioral;
