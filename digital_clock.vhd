library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

entity digital_clock is
  port ( 
KEY : in std_logic_vector (3 downto 0);
 clock: in std_logic; 
 -- clock 50 MHz
 SW: in std_logic_vector(7 downto 0);
 -- 2-bit input used to set the most significant hour digit of the clock 
 -- Valid values are 0 to 2. 
HEX5: out std_logic_vector(6 downto 0);
 -- The most significant digit of the hour. Valid values are 0 to 2 ( Hexadecimal value on 7-segment LED)
 HEX4: out std_logic_vector(6 downto 0);
 -- The most significant digit of the hour. Valid values are 0 to 9 ( Hexadecimal value on 7-segment LED)
 HEX3: out std_logic_vector(6 downto 0);
 -- The most significant digit of the minute. Valid values are 0 to 9 ( Hexadecimal value on 7-segment LED)
 HEX2: out std_logic_vector(6 downto 0);
 -- The most significant digit of the minute. Valid values are 0 to 9 ( Hexadecimal value on 7-segment LED)
 HEX1: out std_logic_vector(6 downto 0);
 HEX0: out std_logic_vector(6 downto 0)
 );
end digital_clock;
architecture rtl of digital_clock is

component hr_u is 
 port( clk, rstb,set: in std_logic;
		h3: in std_logic_vector(3 downto 0);
		 SW: in std_logic_vector(7 downto 0);
        cnt_9hr: out std_logic;
		  hour_u: out std_logic_vector(3 downto 0));
end component;
component hr_t is
port( clk, rstb,set: in std_logic;
			SW: in std_logic_vector(7 downto 0);
        cnt_2hr: out std_logic;
		  hour_t: out std_logic_vector(3 downto 0));
end component;	
component min_t is
port( clk, rstb,set: in std_logic;
			SW: in std_logic_vector(7 downto 0);
        cnt_5m: out std_logic;
		  minute_t: out std_logic_vector(3 downto 0));
end component;
component min_u is
port( clk, rstb,set: in std_logic;
			SW: in std_logic_vector(7 downto 0);
        cnt_9m: out std_logic;
		  minute_u: out std_logic_vector(3 downto 0));
end component;	
component sec_t is
port( clk, rstb,set: in std_logic;
        cnt_5s: out std_logic;
		  second_t: out std_logic_vector(3 downto 0));
end component;			  
component sec_u is
port( clk, rstb,set: in std_logic;
        cnt_9s: out std_logic;
		  second_u: out std_logic_vector(3 downto 0));
end component;	
component clock_delay is
port (
   clk_50hz: in std_logic;
   clk_1sec: out std_logic
  );
end component;	
component clockdisplay is
port (
 B_in: in std_logic_vector(3 downto 0);

 H_out: out std_logic_vector(6 downto 0)
);
end component;	
signal selects,sec1,sec2,min1,min2,hour1,hour2 : std_logic;
signal clk_1sec : std_logic;
signal second1,second2,minute1,minute2,hours1,hours2: std_logic_vector(3 downto 0);

begin
selects <=KEY(0) AND KEY(2) AND KEY(3);
clkdelay:clock_delay port map(clock,clk_1sec);
s1:sec_u port map(clk_1sec,KEY(0), selects,sec1,second1);
s2:sec_t port map(sec1,KEY(0),selects,sec2,second2);
m1:min_u port map(sec2,KEY(0),KEY(3),SW,min1,minute1);
m2:min_t port map(min1,KEY(0),KEY(3),SW,min2,minute2);
h1:hr_u port map(min2,KEY(0),KEY(2),hours2,SW, hour1,hours1);
h2:hr_t port map(hour1,KEY(0),KEY(2),SW, hour2,hours2);
cd0: clockdisplay port map(second1,HEX0);
cd1: clockdisplay port map(second2,HEX1);
cd2: clockdisplay port map(minute1,HEX2);
cd3: clockdisplay port map(minute2,HEX3);
cd4: clockdisplay port map(hours1,HEX4);
cd5: clockdisplay port map(hours2,HEX5);
end rtl;
