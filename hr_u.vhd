library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

entity hr_u is
  port( clk, rstb,set: in std_logic;
			h3: in std_logic_vector(3 downto 0);
		 SW: in std_logic_vector(7 downto 0);

        cnt_9hr: out std_logic;
		  hour_u: out std_logic_vector(3 downto 0));
end hr_u;
architecture rtl of hr_u is
signal s,a :std_logic_vector(3 downto 0);
signal count ,cnt: std_logic_vector(3 downto 0);
signal se ,c: std_logic;
begin
s<=SW(3)&SW(2)&SW(1)&SW(0);
a<="00"&SW(5)&SW(4);
se<= not set;
process(set)
begin
if(((a>="0010" and s>"0011") or(a<"0010" and s>"1001")) and  set='0') then
		c<='0';
		cnt<="0000";
		elsif(s="1001" and set = '0') then
		cnt<=s;
		c<='0';
		else
		cnt<=s;
		c<='0';
	end if;

end process;
--process(se)
--begin
--if(((a>"0010" and s>"0011") or s>"1001") and  se='1') then
--		c<='0';
--		cnt<="0000";
--		elsif(s="1001" and se = '1') then
--		cnt<=s;
--		c<='1';
--		else
--		cnt<=s;
--		c<='0';
--	end if;
--
--end process;
process(clk,rstb,set)
begin
if(rstb='0') then
	count<="0000";
 elsif(set='0') then
    count<=cnt;
	 cnt_9hr<=c;
	 --cnt_9hr<=c;
--    if(((a>"0010" and s>"0011") or s>"1001") and  se='1') then
--		cnt_9hr<='0';
--		count<="0000";
--		elsif(s="1001") then
--		count<=s;
--		cnt_9hr<='1';
--		else
--		count<=s;
--		cnt_9hr<='0';
--		end if;

	 elsif(rising_edge(clk))then
	   if(h3="0010") then
	    if(count>="0011" ) then
			count<="0000";
			cnt_9hr<='1';
			else
				count<=count + '1';
				cnt_9hr<='0';
			end if;
	    elsif(h3<="0010") then
		 if( count>="1001") then
			count<="0000";
			cnt_9hr<='1';
			else
				count<=count + '1';
				cnt_9hr<='0';
			end if;
	end if;
end if;

end process;

hour_u<=count;
end rtl;
