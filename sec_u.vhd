library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

 entity sec_u is
  port( clk, rstb,set: in std_logic;
        cnt_9s: out std_logic;
		  second_u: out std_logic_vector(3 downto 0));
end sec_u;
architecture rtl of sec_u is
signal count : std_logic_vector(3 downto 0);
begin

process(clk,rstb,set)
begin
if(rstb='0') then
	count<="0000";
	cnt_9s<='0';
	
 elsif(set='0') then
    count<="0000";
	 cnt_9s<='0';
	 elsif(rising_edge(clk))then 
	    if(count>="1001") then
			count<="0000";
			cnt_9s<='1';
			else
				count<=count + '1';
				cnt_9s<='0';
			end if;
end if;

end process;
second_u<=count;	
end rtl;
